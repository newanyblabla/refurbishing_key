#!/bin/bash

# toolbox.sh --
#
#   This file is a set of tools allowing the installer
#   to test various computer components : RAM, disk, etc :
#     - memory by Memtester utility : https://github.com/jnavila/memtester
#     - smart by smartctl : https://blog.microlinux.fr/disque-dur-smartctl/
#     - badblocks : https://fr.wikipedia.org/wiki/Badblocks
#     - sensors, sensors-detect : https://github.com/lm-sensors/lm-sensors
#     - 7-Zip : https://openbenchmarking.org/test/pts/compress-7zip-1.10.0
#     - PhotoRec : https://www.cgsecurity.org/wiki/PhotoRec
#     - TestDisk : https://www.cgsecurity.org/wiki/TestDisk
#     - inxi binary : https://github.com/smxi/inxi/tags
#
#   Created on 2010-23 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was tested and validated on Clonezilla live 3.0.1-8 i686/amd64
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

#DEBUG="true"
#BEEP_OFF="true"

DIR_ERASE_LOG=Erase_log

KEY_DRIVE=$(sudo blkid | grep LABEL=\"IMAGES\" | cut  -d ':' -f1)
UEFI_ON=0
SECUREBOOT_ON=0
DATE=`date -u +"%Y-%m-%d-%H-%M"`
LANGUE=$(echo ${LANG} | cut -d _ -f1)

taille_ram_free=$(cat /proc/meminfo | grep MemFree: | cut -d : -f2 | tr -s ' ' | cut -d ' ' -f2)

# Positionnement sur la clé USB au niveau des scripts
# Go to scripts top dir
cd /home/partimag


# Configuration des tests

NUMBER_TEST_ID=14

TEST_OPTIONS[1]="sudo memtester ${taille_ram_free}K 1"
TEST_OPTIONS[2]="sudo smartctl -l error"
TEST_OPTIONS[3]="sudo smartctl -a -T permissive |"
TEST_OPTIONS[4]="sudo badblocks -nvs"

if test -f ./inxi ; then

    TEST_OPTIONS[5]="sudo ./inxi -c 10 -Fxxxza --no-host |"

else

    TEST_OPTIONS[5]="hwinfo --short |"

fi

TEST_OPTIONS[6]="sudo sensors"
TEST_OPTIONS[7]="speaker-test --test wav --nloops 2 --channels 8"
TEST_OPTIONS[8]="sudo systemctl start NetworkManager ; sleep 5 ; nmcli c ; read ; ifconfig ; read ; ping -c 4 google.com ; "
TEST_OPTIONS[9]="sudo systemctl start NetworkManager ; sleep 5 ; nmtui ; clear ; "
TEST_OPTIONS[10]="7z b"
TEST_OPTIONS[11]="7z b -mmt1"
TEST_OPTIONS[12]="sudo photorec"
TEST_OPTIONS[13]="sudo testdisk"
TEST_OPTIONS[14]="sudo sensors-detect"

TEST_TYPE[1]="RAM"
TEST_TYPE[2]="DISK"
TEST_TYPE[3]="DISK"
TEST_TYPE[4]="DISK"
TEST_TYPE[5]="DEVICE"
TEST_TYPE[6]="DEVICE"
TEST_TYPE[7]="DEVICE"
TEST_TYPE[8]="DEVICE"
TEST_TYPE[9]="DEVICE"
TEST_TYPE[10]="DEVICE"
TEST_TYPE[11]="DEVICE"
TEST_TYPE[12]="DISK"
TEST_TYPE[13]="DISK"
TEST_TYPE[14]="DEVICE"

TEST_LONG[1]="1"
TEST_LONG[2]="0"
TEST_LONG[3]="0"
TEST_LONG[4]="1"
TEST_LONG[5]="0"
TEST_LONG[6]="0"
TEST_LONG[7]="0"
TEST_LONG[8]="0"
TEST_LONG[9]="0"
TEST_LONG[10]="0"
TEST_LONG[11]="0"
TEST_LONG[12]="1"
TEST_LONG[13]="1"
TEST_LONG[14]="0"

if [ ${LANGUE} = fr ] ; then

TEST_NAME[1]="Analyse de la mémoire"
TEST_NAME[2]="Analyse des erreurs SMART du disque cible"
TEST_NAME[3]="Données SMART du disque cible"
TEST_NAME[4]="Analyse des secteurs défectueux du disque cible"
TEST_NAME[5]="Information sur le matériel"
TEST_NAME[6]="Analyse des températures de l'ordinateur"
TEST_NAME[7]="Test audio"
TEST_NAME[8]="Test connexion Internet"
TEST_NAME[9]="Activation connexion Internet"
TEST_NAME[10]="Benchmark multi-processeur"
TEST_NAME[11]="Benchmark mono-processeur"
TEST_NAME[12]="Récupération des fichiers perdus sur le disque cible à l'attention les experts"
TEST_NAME[13]="Réparation des tables de partition sur le disque cible à l'attention les experts"
TEST_NAME[14]="Détection des capteurs présents sur l'ordinateur à l'attention les experts"

else

TEST_NAME[1]="Memory analysis"
TEST_NAME[2]="Analyzing Target Disk SMART Errors"
TEST_NAME[3]="SMART data of target disk"
TEST_NAME[4]="Scan bad sectors on target disk"
TEST_NAME[5]="Hardware Information"
TEST_NAME[6]="Computer temperature analysis"
TEST_NAME[7]="Audio test"
TEST_NAME[8]="Internet connection test"
TEST_NAME[9]="Activate Internet connection"
TEST_NAME[10]="Multi-thread benchmark"
TEST_NAME[11]="Single-thread benchmark"
TEST_NAME[12]="Recovering lost files on target disk for experts"
TEST_NAME[13]="Repairing partition tables on target disk for experts"
TEST_NAME[14]="Detection of sensors present on the computer for experts"

fi

# Test de la présence de la batterie

detection_batterie=$(acpi -b)

if [[ ${detection_batterie} != "" ]] ; then

    NUMBER_TEST_ID=$((${NUMBER_TEST_ID}+1))

    TEST_OPTIONS[${NUMBER_TEST_ID}]="acpi -bti |"

    TEST_TYPE[${NUMBER_TEST_ID}]="DEVICE"

    TEST_LONG[${NUMBER_TEST_ID}]="0"

    if [ ${LANGUE} = fr ] ; then

    TEST_NAME[${NUMBER_TEST_ID}]="Information sur la batterie"

    else

    TEST_NAME[${NUMBER_TEST_ID}]="Battery information"

    fi

fi

#####################################################

CONFIG_FILE=clone.ini
YELLOW='\033[1;33m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
RED='\033[0;31m'
NC='\033[0m' # No Color

# Lecture des options de la ligne de commande
# Read command line options
LINE_OPTION=$1

if [ -z ${DEBUG} ] ; then
    # Efface l'affichage précédant
    clear
fi

if [[ ${LINE_OPTION} == "-a" ]] ; then
    AUTOMATIC="true"
fi


# Test Secureboot
if test -d /sys/firmware/efi/ ; then
    if [ "$(which mokutil)" != "" ] ; then
        if [ "$(mokutil --sb-state | grep "SecureBoot enabled")" != "" ] ; then
            SECUREBOOT_ON=1
        fi
    fi
fi


if [ ${LANGUE} = fr ]
then

    message_info_emmabuntus="Clé USB de réemploi par Emmabuntüs (https://emmabuntus.org)"
    message_info_sources="Les sources sont disponibles sur http://usb-reemploi.emmabuntus.org"
    message_identification_usb_ko="Attention : La clé USB avec l'étiquette \"IMAGES\" n'a pas été indentifiée. Taper sur une touche quelconque pour sortir du script"
    message_trouve_usb_avant="Clé USB trouvée sur"
    message_trouve_usb_apres=", mais ce n'est pas son emplacement habituel, ce qui signifie que vous avez plusieurs disques durs sur ce système."
    message_selection_usb="Veuillez entrer le numéro correspondant au disque cible de l'installation :"
    message_mauvais_nombre_disque_dur="Attention : Vous n'avez pas entré un nombre pour selectionner le disque. Taper sur une touche quelconque pour sortir du script"
    message_mauvais_selection_disque_dur="Attention : Vous n'avez pas entré un nombre correspondant à un disque. Taper sur une touche quelconque pour sortir du script"
    message_usb_disque_dur_avant="Clé USB trouvée sur"
    message_usb_disque_dur_apres="et disque sélectionné sur "
    message_type_disque_dur="Magnétique"
    message_mode_test="Tests prédéfinis pour la clé de réemploi :"
    message_info_test="/!\ Attention : Les tests en couleur \"orange\" peuvent prendre beaucoup de temps.\nPour les interrompre tapez CTRL+C.\nSi vous sortez du script tapez \"exit\" pour reprendre le script ;)"
    message_choix_test="Veuillez sélectionner le test désiré ?"
    message_nombre="Saisissez un nombre ci-dessus, puis validez : "
    commande_executee="Commande exécutée "
    message_inxi="Remarque : Si vous désirez utiliser l'utilitaire inxi à la place de hwinfo,\nveuillez mettre le binaire de inxi à côté de ce script sur la clé USB\net le script utilisera inxi à la place de hwinfo"
    message_sensors_detect="/!\ Attention : Les auteurs ont fait de leur mieux pour rendre cette détection aussi sûre que possible, et de fait elle fonctionne très bien dans la plupart des cas, mais il est impossible de garantir que la détection des capteurs ne bloquera pas ni même ne détruira pas un système spécifique. Donc, en règle générale, vous ne devriez pas utiliser la détection des capteurs sur des serveurs de production ni lorsque vous ne pouvez vous permettre de remplacer une partie quelconque de votre système. Il est également recommandé de ne pas forcer une étape de détection qui aurait été ignorée par défaut, à moins que vous ne sachiez parfaitement ce que vous faites."
    message_pause_continue_script="Appuyez sur la touche \"Entrée\" pour continuer le script"
    message_stop_computer="Appuyez sur la touche \"Entrée\" pour arrêter l'ordinateur"
    message_start_test="Ou appuyer sur la touche \"T\" pour relancer un test,\n\"S\" pour lancer le clonage en mode semi-automatique ou sur \"A\" pour le mode automatique : "

else

    message_info_emmabuntus="Emmabuntüs refurbishing USB key (https://emmabuntus.org)"
    message_info_sources="Sources are available on http://usb-reemploi.emmabuntus.org"
    message_identification_usb_ko="Warning : The USB key with the \"IMAGES\" label was not found. Press any key to exit the script"
    message_trouve_usb_avant="USB Key found on"
    message_trouve_usb_apres=", but this is not it's usual location, which means that there are several hard drives on your computer."
    message_selection_usb="Please enter the number corresponding to this install target disk of ."
    message_mauvais_nombre_disque_dur="Warning : You did not enter a number to select the disk. Press any key to exit"
    message_mauvais_selection_disque_dur="Warning : You did not enter a number corresponding to a disk. Press any key to exit"
    message_usb_disque_dur_avant="USB Key found on"
    message_usb_disque_dur_apres="and selected hard drive on "
    message_type_disque_dur="Magnetic"
    message_mode_test="Predefined tests for reuse key: "
    message_info_test="/!\ Warning: Tests in \"orange\" color can take a long time.\nTo interrupt them type CTRL+C.\nIf you exit the script type \"exit\" to resume the script ;)"
    message_choix_test="Please select the desired test ?"
    message_nombre="Enter one number listed above, then validate: "
    commande_executee="Command executed "
    message_inxi="Note: If you want to use the inxi utility instead of hwinfo,\nplease put the inxi binary next to this script on the USB drive\nand then inxi will be used instead of hwinfo"
    message_sensors_detect="/!\ Warning: The authors made their best to make the detection as safe as possible, and it turns out to work just fine in most cases, however it is impossible to guarantee that sensors-detect will not lock or kill a specific system. So, as a rule of thumb, you should not run sensors-detect on production servers, and you should not run sensors-detect if can't afford replacing a random part of your system. Also, it is recommended to not force a detection step which would have been skipped by default, unless you know what you are doing."
    message_pause_continue_script="Press the \"Enter\" key to continue the script"
    message_stop_computer="Press the \"Enter\" key to shut down the system"
    message_start_test="Or press the \"T\" key to launch another test,\n\"S\" to start cloning in semi-automatic mode or \"A\" to use the automatic mode: "

fi

function affiche_commande()
{

    commande_affiche=${1}
    efface=${2}

    if [[ $(echo ${commande_affiche} | grep "sleep") ]] ; then

        duree=$(echo ${commande_affiche} | cut -d ' ' -f2)

        if [ ${LANGUE} = fr ] ; then
            message_attente="Attente de ${duree} secondes pour démarrage du service"
        else
            message_attente="Wait ${duree} seconds for service to start"
        fi

        echo "${message_attente}"
        echo ""

    elif [[ $(echo ${commande_affiche} | grep "read") ]] ; then

        echo ""
        echo -e "${YELLOW}${message_pause_continue_script}${NC}"

    else

        if [ ${efface} == "1" ] ; then
            clear
        fi

        echo ""
        echo -e "${GREEN}##################################################################################${NC}"
        echo -e " ${YELLOW}${commande_executee}${NC} : ${ORANGE}${commande_affiche}${NC}"
        echo -e "${GREEN}##################################################################################${NC}"
        echo ""

        if [[ $(echo ${commande_affiche} | grep "hwinfo") ]] ; then

            echo -e "${YELLOW}${message_inxi}${NC}"
            echo ""

            sleep 3

        fi

        if [[ $(echo ${commande_affiche} | grep "sensors-detect") ]] ; then

            echo -e "${YELLOW}${message_sensors_detect}${NC}"
            echo ""

            sleep 3

        fi

    fi

}

echo ""
echo ${message_info_emmabuntus}
echo ${message_info_sources}
echo ""

# Verrouillage du pavé numérique
setleds -D +num

# Choix des tests

echo -e ""
echo -e "${message_mode_test}"
echo -e ""

for (( test_id = 1 ; test_id <= ${NUMBER_TEST_ID} ; test_id++ ))
do
    if [ ${TEST_LONG[test_id]} = "1" ] ; then
        couleur_test=${ORANGE}
    else
        couleur_test=${GREEN}
    fi

    if [ ${test_id} -lt 10 ] ; then
        echo -e "   ${YELLOW}${test_id}${NC} - ${couleur_test}${TEST_NAME[${test_id}]}${NC}"
    else
        echo -e "  ${YELLOW}${test_id}${NC} - ${couleur_test}${TEST_NAME[${test_id}]}${NC}"
    fi
done

echo ""
echo -e "${ORANGE}${message_info_test}${NC}"
echo ""
echo -e -n "${YELLOW}${message_choix_test}${NC} "

echo -e -n "${ORANGE}"
read test_id_number
echo -e -n "${NC}"

# Test si test_id_number est un nombre
if [[ "${test_id_number}" == ?(-)+([0-9]) ]] ; then

if [ ${test_id_number} -ge 1 ] && [ ${test_id_number} -le ${NUMBER_TEST_ID} ] ; then

    if [ ${TEST_TYPE[test_id_number]} = "RAM" ] ; then

        clear

        echo ""
        echo -e "${GREEN}##################################################################################${NC}"
        free -m
        echo -e "${GREEN}##################################################################################${NC}"
        echo ""

        affiche_commande "${TEST_OPTIONS[test_id_number]}" 0

        ${TEST_OPTIONS[test_id_number]}

    elif [ ${TEST_TYPE[test_id_number]} = "DEVICE" ] ; then

        echo ""

        if [[ $(echo ${TEST_OPTIONS[test_id_number]} | grep ";") ]] ; then

            commande=""

            for i in $(echo ${TEST_OPTIONS[test_id_number]})
            do
                if [[ ${i} == ";" ]] ; then

                    affiche_commande "${commande}" 1

                    ${commande}
                    commande=""
                else
                    commande="${commande} ${i}"
                fi
            done

        elif [[ $(echo ${TEST_OPTIONS[test_id_number]} | grep "|") ]] ; then

            commande=""

            for i in $(echo ${TEST_OPTIONS[test_id_number]})
            do
                if [[ ${i} == "|" ]] ; then

                    affiche_commande "${commande}" 1

                    sleep 1

                    ${commande} | more -d -e
                    commande=""
                else
                    commande="${commande} ${i}"
                fi
            done

        else

            affiche_commande "${TEST_OPTIONS[test_id_number]}" 1

            ${TEST_OPTIONS[test_id_number]}
        fi

    else

        # Identification de la clé USB
        # Finding out the USB Key
        if [ -z ${KEY_DRIVE} ] ; then

        echo -e -n "${RED}${message_identification_usb_ko}${NC}"
        echo -e "\a"

        read read_no_correct_label_key

        exit 1

        elif [ $(echo ${KEY_DRIVE} | grep sdb) ] ; then

            KEY_DRIVE_FOUND="/dev/sdb"

        elif [ $(echo ${KEY_DRIVE} | grep sda) ] ; then

            KEY_DRIVE_FOUND="/dev/sda"

        fi

        # Identification du disque dur
        # Finding the hard drive

        find_disk=$(sudo fdisk -l | grep 'Disk /dev/sd\|Disk /dev/hd\|Disk /dev/mmcblk\|Disk /dev/hd\|Disk /dev/nvme' | cut -d : -f1 | cut -d " " -f2)
        nb_find_disk=$(echo ${find_disk} | wc -w)

        if [ ! -z ${DEBUG} ] ; then
            echo -e "KEY_DRIVE_FOUND=${KEY_DRIVE_FOUND}"
            echo -e "find_disk=${find_disk}"
            echo -e "nb_find_disk=${nb_find_disk}"
        fi

        if [ ${nb_find_disk} -eq 2 ] && [ -n ${KEY_DRIVE_FOUND}  ] ; then # Si 2 disques trouvés et que la clé USB a été identifiée

            for disk_name in ${find_disk}
            do
                if [ "${disk_name}" != ${KEY_DRIVE_FOUND} ] ; then
                    HARD_DRIVE=$(echo ${disk_name} | cut -d / -f3)
                fi

            done

        elif [ -n "${KEY_DRIVE}" ] ; then

            echo -e "${message_trouve_usb_avant} ${GREEN}${KEY_DRIVE}${NC}${message_trouve_usb_apres}"
            echo ""
            echo "${message_selection_usb}"

            i=0
            for disk_name in ${find_disk}
            do
                if [ ! $(echo ${KEY_DRIVE} | grep ${disk_name}) ] ; then
                    disk_model=$(sudo fdisk -l ${disk_name} | grep "Disk model" | cut -d : -f2)
                    disk_size=$(sudo fdisk -l ${disk_name} | grep "Disk " | grep sector | cut -d : -f2 | cut -d B -f1)
                    i=$(($i+1))
                    echo -e "${YELLOW}${i}${NC} - ${GREEN}${disk_name} - model: ${disk_model} - size: ${disk_size}B${NC}"
                fi

            done

            echo ""
            echo -e -n "${YELLOW}${message_nombre}${NC}"

            echo -e -n "${ORANGE}"
            read read_disk_number
            echo -e -n "${NC}"

            # Test si c'est un nombre
            # Check if the entry is a number
            if echo ${read_disk_number} | grep -qE '^[0-9]+$'; then

                echo ""

            else

                echo -e -n "${RED}${message_mauvais_nombre_disque_dur}${NC}"
                echo -e "\a"

                read read_no_correct_number

                exit 2
            fi

            i=0
            macth_number=0

            for disk_name in ${find_disk}
            do
                if [ ! $(echo ${KEY_DRIVE} | grep ${disk_name}) ] ; then
                    i=$(($i+1))
                    if [ ${read_disk_number} -eq $i ] ; then
                        HARD_DRIVE=${disk_name}
                        macth_number=1
                    fi
                fi
            done

            if [  ${macth_number} -eq 1 ] ; then

                HARD_DRIVE=$(echo ${HARD_DRIVE} | cut -d / -f3)

            else

                echo -e -n "${RED}${message_mauvais_selection_disque_dur}${NC}"
                echo -e "\a"

                read read_no_correct_number

                exit 3

            fi

        fi

        # Identification du type de disque dur : Classique ou SSD
        TYPE_HARD_DRIVE=$(cat /sys/block/${HARD_DRIVE}/queue/rotational)
        if [ ${TYPE_HARD_DRIVE} -eq 0 ] ; then

            # Test si le SSD est NVMe
            HARD_DRIVE_NVME=$(sudo nvme list | grep "/dev/" | cut -d " " -f1 )

            if [ "${HARD_DRIVE_NVME}" != "" ] ; then

                NAME_TYPE_HARD_DRIVE="MVMe"
                ERASE_LOG=${NVME_LOG}

            else

                NAME_TYPE_HARD_DRIVE="SSD"
                ERASE_LOG=${HDPARM_LOG}

            fi

        else
            # Disque magnétique
            NAME_TYPE_HARD_DRIVE=${message_type_disque_dur}
            ERASE_LOG=${NWIPE_LOG}
        fi

        echo ""
        echo -e "${message_usb_disque_dur_avant} ${GREEN}${KEY_DRIVE}${NC} ${message_usb_disque_dur_apres}${GREEN}/dev/${HARD_DRIVE}${NC} (Type : ${GREEN}${NAME_TYPE_HARD_DRIVE}${NC})${NC}"
        echo ""

        # lancement du test
        echo ""

        if [[ $(echo ${TEST_OPTIONS[test_id_number]} | grep "|") ]] ; then

            commande=""

            for i in $(echo ${TEST_OPTIONS[test_id_number]})
            do
                if [[ ${i} == "|" ]] ; then

                    affiche_commande "${commande} /dev/${HARD_DRIVE}" 1

                    sleep 1

                    ${commande} /dev/${HARD_DRIVE} | more -d -e
                    commande=""
                else
                    commande="${commande} ${i}"
                fi
            done

        else

            affiche_commande "${TEST_OPTIONS[test_id_number]} /dev/${HARD_DRIVE}" 1

            ${TEST_OPTIONS[test_id_number]} /dev/${HARD_DRIVE}

        fi

    fi

    if [ ${TEST_LONG[test_id_number]} = "1" ] ; then
        echo -e -n "\a"
    fi

fi

fi

# Relancement du test ou arrêt
echo ""
echo -e "${ORANGE}${message_stop_computer}${NC}"
echo -e -n "${YELLOW}${message_start_test}${NC}"
echo -e -n "${ORANGE}"
read mode_clonage
echo -e -n "${NC}"

if [ "$mode_clonage" = "t" ] || [ "$mode_clonage" = "T" ] ; then
    ./toolbox.sh
elif [ "$mode_clonage" = "s" ] || [ "$mode_clonage" = "S" ] ; then
    ./clone.sh
elif [ "$mode_clonage" = "a" ] || [ "$mode_clonage" = "A" ] ; then
    ./clone.sh -a
else
    shutdown --poweroff now
fi
